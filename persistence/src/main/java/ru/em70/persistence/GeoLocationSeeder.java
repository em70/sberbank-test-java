package ru.em70.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ru.em70.domain.GeoLocation;

import java.util.ArrayList;
import java.util.List;

@Component
public class GeoLocationSeeder implements CommandLineRunner {

    private final GeoLocationRepository geoLocationRepository;


    @Autowired
    public GeoLocationSeeder(GeoLocationRepository geoLocationRepository) {
        this.geoLocationRepository = geoLocationRepository;
    }

    @Override
    public void run(String... args) {
        GeoLocation tomsk = new GeoLocation("Томск", 84.58, 56.29, "Томская область");
        GeoLocation novosibirsk = new GeoLocation("Новосибирск", 82.56, 55.02, "Новосибирская область");
        GeoLocation krasnodar = new GeoLocation("Красноярск", 92.52, 56.01, "Красноярский край");

        List<GeoLocation> cities = new ArrayList<>();

        cities.add(tomsk);
        cities.add(novosibirsk);
        cities.add(krasnodar);

        this.geoLocationRepository.saveAll(cities);
    }
}
