package ru.em70.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.em70.domain.WeatherRequest;


@Repository
public interface WeatherRequestRepository extends JpaRepository<WeatherRequest, Long> {
    public WeatherRequest findById(String id);
}
