package ru.em70.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.em70.domain.GeoLocation;

import java.util.List;

@Repository
public interface GeoLocationRepository extends JpaRepository<GeoLocation, Long> {

    public List<GeoLocation> findByLatitudeAndLongitude(double latitude, double longitude);

}
