package ru.em70.domain;


import javax.persistence.*;

@Entity
@Table(name = "geo_location")
public class GeoLocation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    public long getId() {
        return id;
    }

    private String city; // simplified
    private double longitude;
    private double latitude;
    private String region; // simplified

    protected GeoLocation() {
    }

    public GeoLocation(String city, double longitude, double latitude, String region) {

        this.city = city;
        this.longitude = longitude;
        this.latitude = latitude;
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getRegion() {
        return region;
    }

}
