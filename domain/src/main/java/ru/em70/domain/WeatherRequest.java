package ru.em70.domain;


import javax.persistence.*;

@Entity
@Table(name = "weather_request")
public class WeatherRequest {
    @Id
    private String id;
    private String username;

    protected WeatherRequest() {
    }

    public WeatherRequest(String id, String username) {
        this.id = id;
        this.username = username;
    }

    /**
     * WeatherRequest id
     *
     * @return
     */
    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }
}
