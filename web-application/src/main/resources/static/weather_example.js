var res = {
    "coordinate": {
        "latitude": 102.0,
        "longitude": 45.0
    }
    ,
    "wind":
        {
            "speed": 28.15177204291067,
            "deg": 46.38032443456645
        }
    ,
    "main":
        {
            "temp": 0.0,
            "humidity": 0.0,
            "pressure": 0.0,
            "tempMin": 0.0,
            "tempMax": 0.0
        }
};