!function () {

    var stompClient = null;

    function setConnected(connected) {
        connected = !!connected;
        $("#connect").prop("disabled", connected);
        $("#disconnect").prop("disabled", !connected);
        if (connected) {
            $("#conversation").show();
        }
        else {
            $("#conversation").hide();
        }
        $("#requests").html("");
    }

    function connect() {
        var socket = new SockJS('/ws');
        stompClient = Stomp.over(socket);
        stompClient.connect(
            {},
            function (frame) { // on connect
                setConnected(true);
                console.log('Connected: ' + frame);
                stompClient.subscribe('/user/weather/result', function (weatherResult) {
                    var res = JSON.parse(weatherResult.body);
                    console.log("/user/weather/result: ", res);
                    showRequestResult(res);
                });
                stompClient.subscribe('/user/weather/error', function (serverResponse) {
                    var res = JSON.parse(serverResponse.body);
                    console.log("/user/weather/error: ", res);
                    showRequestResult(res);
                });

            },
            function (message) { // error callback
                disconnect();
                console.log(message);
                showError(message);
            }
        );
    }

    function disconnect() {
        if (stompClient !== null) {
            stompClient.disconnect();
        }
        setConnected(false);
        console.log("Disconnected");
    }

    function sendReq() {
        stompClient.send("/app/weather/get", {}, JSON.stringify({
            'latitude': $("#latitude").val(),
            'longitude': $("#longitude").val()
        }));
    }


    function showRequestResult(result) {
        var $requests = $("#requests");
        if (typeof result.position !== 'undefined') {
            $requests
                .append(
                    "<tr>" +
                    "<td> Position" +
                    "</td>" +
                    "<td>" +
                    result.position +
                    "</td>" +
                    "</tr>"
                );
        } else {
            $requests.append(
                "<tr>" +
                "<td> temperature: " +
                result.main.temp +
                "</td>" +
                "<td>wind: " +
                result.wind.speed +
                "</td>" +
                "</tr>"
            );
        }
    }


    function showError(result) {
        var $requests = $("#requests");
        $requests.append(
            "<tr>" +
            "<td style='color: red;'> Error occured: " +
            "</td>" +
            "<td>" +
            result +
            "</td>" +
            "</tr>"
        );
    }

    $(function () {
        $("form").on('submit', function (e) {
            e.preventDefault();
        });
        $("#connect").click(function () {
            connect();
        });
        $("#disconnect").click(function () {
            disconnect();
        });
        $("#send").click(function () {
            sendReq();
        });
    });
}();
