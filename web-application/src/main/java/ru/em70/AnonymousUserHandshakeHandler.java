package ru.em70;

import org.springframework.http.server.ServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import java.security.Principal;
import java.util.Map;

/**
 * Performs  initial validation of the WebSocket handshake request.
 * Generates anonymous user to make possible authenticate them and send private messages.
 */
public class AnonymousUserHandshakeHandler extends DefaultHandshakeHandler {

    /**
     * A method is used to generate anonymous user and associate
     * them with the current WebSocket session if the user is not authenticated
     *
     * @param request    the handshake request
     * @param wsHandler  the WebSocket handler that will handle messages
     * @param attributes handshake attributes to pass to the WebSocket session
     * @return the user for the WebSocket session
     */
    @Override
    protected Principal determineUser(
            ServerHttpRequest request,
            WebSocketHandler wsHandler,
            Map<String, Object> attributes
    ) {
        Principal principal = request.getPrincipal();
        if (principal == null) {
            principal = new AnonymousPrincipal();
        }

        return principal;
    }

}
