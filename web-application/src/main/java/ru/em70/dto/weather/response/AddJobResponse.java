package ru.em70.dto.weather.response;

/**
 * Represents server response message on client weather request
 */
public class AddJobResponse {
    private String id;
    private long position;

    /**
     * Creates response
     * @param id request id
     * @param position queue position
     */
    public AddJobResponse(String id, long position) {
        this.id = id;
        this.position = position;
    }

    public String getId() {
        return id;
    }

    public long getPosition() {
        return position;
    }
}
