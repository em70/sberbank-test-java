package ru.em70.dto.weather.request;


/**
 * Client request message representation class
 */
public class AddJobRequest {

    private double longitude;
    private double latitude;

    protected AddJobRequest (){}

    /**
     * Creates request
     * @param longitude geo coordinate
     * @param latitude geo coordinate
     */
    public AddJobRequest(double longitude, double latitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * Get latitude geo coordinate
     * @return latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Get longitude geo coordinate
     * @return longitude
     */
    public double getLongitude() {
        return longitude;
    }
}

