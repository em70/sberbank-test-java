package ru.em70.configuration;


import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import ru.em70.AnonymousUserHandshakeHandler;


/**
 * WebSocket configuration class
 * Defines methods for configuring message handling with STOMP from WebSocket clients.
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfiguration implements WebSocketMessageBrokerConfigurer {


    /**
     * Configures message broker options by setting destination prefixes.
     * Client side ( e.g. browser) should make subscriptions on channels using these prefixes:
     * - /app/weather/ - send data to the server
     * - /user/weather - receive message from the server. Used for private channels. E.g.: /user/weather/result
     *
     * @param registry A registry for configuring message broker options.
     */
    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/weather");
        registry.setApplicationDestinationPrefixes("/app/"); // path to controllers (request data)
        registry.setUserDestinationPrefix("/user/");
    }

    /**
     * Registers STOMP endpoint mapping to a URL: "ws"
     * Client side ( e.g. browser) code example: <code>var  ws = new SockJS("ws");</code>
     *
     * @param registry A contract for registering STOMP over WebSocket endpoints.
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws") // on the client side:
                .setHandshakeHandler(new AnonymousUserHandshakeHandler())
                .setAllowedOrigins("*")
                .withSockJS();
    }

}
