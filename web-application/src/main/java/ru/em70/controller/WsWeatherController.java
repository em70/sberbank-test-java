package ru.em70.controller;


import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import ru.em70.domain.GeoLocation;


import ru.em70.domain.WeatherRequest;
import ru.em70.dto.weather.request.AddJobRequest;
import ru.em70.dto.weather.response.AddJobResponse;
import ru.em70.exception.GeoLocationNotFoundException;
import ru.em70.service.GeoLocationService;
import ru.em70.service.WeatherRequestService;
import ru.em70.services.TaskInfo;
import ru.em70.services.weather.WeatherService;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import java.security.Principal;

/**
 * WebSocket controller.
 * Defines methods for weather service
 */
@Controller
public class WsWeatherController {

    private final WeatherService weatherService;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final GeoLocationService geoLocationService;
    private final WeatherRequestService weatherRequestService;

    /**
     * Creates controller
     *
     * @param weatherService        weather service instance
     * @param simpMessagingTemplate provides methods for sending messages to a user
     * @param geoLocationService    provides methods for working with geo location
     * @param weatherRequestService provides methods for working with WeatherRequest
     */
    public WsWeatherController(
            WeatherService weatherService,
            SimpMessagingTemplate simpMessagingTemplate,
            GeoLocationService geoLocationService,
            WeatherRequestService weatherRequestService
    ) {
        this.weatherService = weatherService;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.geoLocationService = geoLocationService;
        this.weatherRequestService = weatherRequestService;
    }

    /**
     * Adds client request to the queue of requests and notifies client with result (request id and number in queue)
     * via STOMP
     *
     * @param principal implementation of java Principal
     * @param request   client request dto object
     * @throws Exception
     */
    @MessageMapping("/weather/get")
    public void addJob(Principal principal, AddJobRequest request) throws Exception {
        GeoLocation location = this.geoLocationService.findLocationByCoordinates(request.getLatitude(), request.getLongitude());
        if (location == null) {
            throw new GeoLocationNotFoundException("there is no location with such coordinates");
        }
        String jobId = weatherService.requestWeather(location);
        TaskInfo taskInfo = weatherService.getTaskInfo(jobId);
        WeatherRequest weatherRequest = new WeatherRequest(jobId.toString(), principal.getName());
        this.weatherRequestService.saveRequest(weatherRequest);
        AddJobResponse response = new AddJobResponse(jobId, taskInfo.getNumberInQueue());
        this.simpMessagingTemplate.convertAndSendToUser(principal.getName(), "/weather/result", response);
    }

    /**
     * Subscribes on JMS notification from WeatherService and notifies user about request result via STOMP
     *
     * @param result incoming JMS message
     */
    @JmsListener(destination = "weather_result", containerFactory = "connectionFactory")
    public void receiveMessage(MapMessage result) throws Exception {
        try {
            WeatherRequest weatherRequest = this.weatherRequestService.retreiveRequestById(result.getString("taskId"));
            if (weatherRequest == null) {
                return; // nothing to do  - we do not know user
            }
            this.simpMessagingTemplate.convertAndSendToUser(weatherRequest.getUsername(), "/weather/result", result.getString("result"));
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            throw e;
        }
    }


    /**
     * Throw any exceptions caused by STOMP to the end user
     * on the /user/weather/error destination
     *
     * @param exception Exception instance
     * @return text representation of the error
     */
    @MessageExceptionHandler
    @SendToUser("/weather/error")
    public String handleException(Throwable exception) {
        return "error";
    } // use this stub to prevent remote user client from service implementation details

}
