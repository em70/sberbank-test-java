package ru.em70.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.em70.domain.GeoLocation;
import ru.em70.persistence.GeoLocationRepository;

import java.util.List;

/**
 * Rest API controller.
 * Defines methods for geo location resource representation
 */
@CrossOrigin
@RestController
public class GeoLocationsController {

    private final GeoLocationRepository geoLocationRepository;

    /**
     * Constructor
     *
     * @param geoLocationRepository repository
     */
    public GeoLocationsController(GeoLocationRepository geoLocationRepository) {
        this.geoLocationRepository = geoLocationRepository;
    }

    /**
     * Represents /geo-locations HTTP resource
     *
     * @return list of GeoLocation
     */
    @GetMapping("/geo-locations")
    public List<GeoLocation> getGetLocations() {
        return this.geoLocationRepository.findAll();
    }
}
