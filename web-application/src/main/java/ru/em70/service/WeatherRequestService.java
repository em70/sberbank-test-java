package ru.em70.service;


import org.springframework.stereotype.Service;
import ru.em70.domain.WeatherRequest;
import ru.em70.persistence.WeatherRequestRepository;


/**
 * Weather request service
 * Implements business logic for location processes
 */
@Service
public class WeatherRequestService {

    private final WeatherRequestRepository weatherRequestRepository;

    /**
     * Creates service
     *
     * @param weatherRequestRepository repository
     */
    public WeatherRequestService(WeatherRequestRepository weatherRequestRepository) {
        this.weatherRequestRepository = weatherRequestRepository;
    }


    /**
     * Saves weather request to the persistence layer
     *
     * @param weatherRequest Weather request
     */
    public void saveRequest(WeatherRequest weatherRequest) {
        weatherRequestRepository.save(weatherRequest);
    }


    /**
     * Retreives request from persistence layer
     *
     * @param id Job identifier
     * @return weather request (dto object)
     */
    public WeatherRequest retreiveRequestById(String id) {
        return this.weatherRequestRepository.findById(id);
    }

}
