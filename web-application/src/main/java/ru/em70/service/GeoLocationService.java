package ru.em70.service;


import org.springframework.stereotype.Service;
import ru.em70.domain.GeoLocation;
import ru.em70.persistence.GeoLocationRepository;

import java.util.List;

/**
 * GeoLocation service
 * Implements business logic for location processes
 */
@Service
public class GeoLocationService {

    private final GeoLocationRepository geoLocationRepository;

    /**
     * Creates service
     *
     * @param geoLocationRepository repository
     */
    public GeoLocationService(GeoLocationRepository geoLocationRepository) {
        this.geoLocationRepository = geoLocationRepository;
    }

    /**
     * Searches geo location by coordinates
     *
     * @param latitude  latitude geographics coordinate
     * @param longitude longitude geographics coordinate
     * @return GeoLocation or null
     */
    public GeoLocation findLocationByCoordinates(double latitude, double longitude) {
        GeoLocation location = null;
        List<GeoLocation> locationList = this.geoLocationRepository.findByLatitudeAndLongitude(latitude, longitude);
        if (locationList.size() > 0) {
            location = locationList.listIterator().next();
        }
        return location;
    }

}
