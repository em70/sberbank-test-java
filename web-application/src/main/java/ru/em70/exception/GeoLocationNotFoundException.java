package ru.em70.exception;

public class GeoLocationNotFoundException extends Exception {
    public GeoLocationNotFoundException(String message) {
        super(message);
    }
}
