package ru.em70;

import java.security.Principal;
import java.util.Objects;
import java.util.Random;


/**
 * Implements standard java.security.Principal interface
 * Represents anonymous user.
 * Should be used in authentication process of anonymous user.
 */
public class AnonymousPrincipal implements Principal {

    final private String[] ADJECTIVES = {
            "callous", "candid", "cantankerous", "capable", "careful", "careless", "caustic"
            , "cautious", "charming", "childish", "childlike", "cheerful", "chic", "churlish", "circumspect"
            , "civil", "clean", "clever", "clumsy", "coherent", "cold", "competent", "composed"
            , "conceited", "condescending", "confident", "confused", "conscientious"
    };

    final private String[] NOUNS = {
            "calm", "cell", "chinook", "cirriform", "cirrus",
            "climate", "climatology", "cloud", "cloud-bank", "cloudburst",
            "cloudy", "cold", "cold-front", "cold-snap", "cold-wave",
            "compass", "condensation", "contrail", "convergence",
            "cumuliform", "cumulonimbus", "cumulus", "current"
    };

    final private String getRandom(String[] array) {
        int random = getRandomInt(array.length);
        return array[random];
    }


    final private int getRandomInt(int bound) {
        return new Random().nextInt(bound);
    }


    final private String generateName() {
        return this.getRandom(ADJECTIVES) + "-" + this.getRandom(NOUNS) + "-" + getRandomInt(Integer.MAX_VALUE);
    }

    private String name;

    /**
     * Constructor. Automatically generates the principal name
     */
    public AnonymousPrincipal() {
        this.name = this.generateName();
    }


    @Override
    public String getName() {
        return name;
    }

    /**
     * Compares objects
     * @param another object to comparison
     * @return true if object is instance of Principal and the names are equal.
     */
    @Override
    public boolean equals(Object another) {
        if (!(another instanceof Principal))
            return false;

        Principal principal = (Principal) another;
        return principal.getName().equals(this.name);

    }

    /**
     * Returns a hash code value for this Principal.
     * @return a hash code value for the Principal name.
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.name);
    }

}
