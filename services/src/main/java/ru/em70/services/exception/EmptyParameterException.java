package ru.em70.services.exception;

public class EmptyParameterException extends Exception {
    public EmptyParameterException() {
        super("Empty task parameter");
    }

    public EmptyParameterException(String message) {
        super(message);
    }
}
