package ru.em70.services.exception;

public class TaskAddingException extends Exception {

    public TaskAddingException() {
        super("Could not queue the task");
    }

    public TaskAddingException(String message) {
        super(message);
    }
}
