package ru.em70.services.exception;

public class IncorrectParametersException extends Exception {
    public IncorrectParametersException() {
        super("Incorrect parameters");
    }

    public IncorrectParametersException(String message) {
        super(message);
    }
}
