package ru.em70.services;


/**
 * Represents information about task
 */
public class TaskInfo {
    private String sessionId;
    private long numberInQueue;

    /**
     * Creates task info
     * @param sessionId task identifier
     * @param numberInQueue position in the queue
     */
    public TaskInfo(String sessionId, long numberInQueue) {
        this.sessionId = sessionId;
        this.numberInQueue = numberInQueue;
    }

    public String getSessionId() {
        return sessionId;
    }

    public long getNumberInQueue() {
        return numberInQueue;
    }
}
