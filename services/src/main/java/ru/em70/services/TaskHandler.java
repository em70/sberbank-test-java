package ru.em70.services;

/**
 * Task handler interface
 */
public interface TaskHandler {
    String runJob(Task task) throws Exception;
    TaskInfo getJobInfo(String sessionId) throws Exception;
}
