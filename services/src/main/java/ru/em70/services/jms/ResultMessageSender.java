package ru.em70.services.jms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import ru.em70.services.TaskResult;

import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;
import java.util.HashMap;
import java.util.Map;

@Component
public class ResultMessageSender {

    @Bean
    public JmsListenerContainerFactory<?> connectionFactory(
            ConnectionFactory connectionFactory,
            DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        return factory;
    }


    @Autowired
    private ConnectionFactory connectionFactory;
    private JmsTemplate jmsTemplate;

    @PostConstruct
    public void init() {
        this.jmsTemplate = new JmsTemplate(connectionFactory);
    }

    public void sendMessage(TaskResult result) {
        Map<String, Object> map = new HashMap<>();
        map.put("taskId", result.getTaskId());
        map.put("result", result.getResult());
        jmsTemplate.convertAndSend("weather_result", map);
    }
}
