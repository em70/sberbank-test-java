package ru.em70.services.jms;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.MapMessage;

@Component
public class ReceiverExample {
    // Uncomment next line for testing notifications
    // @JmsListener(destination = "weather_result", containerFactory = "connectionFactory")
    public void receiveMessage(MapMessage result) {
        try {
            System.out.println("Task result: " + result.getString("taskId") + result.getString("result"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
