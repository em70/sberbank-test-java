package ru.em70.services.weather;

public interface WeatherApi {
    String getWeatherByPosition(double lat, double lon);

    String getWeatherByCityName(String cityName);
}
