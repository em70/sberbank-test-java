package ru.em70.services.weather;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import ru.em70.domain.GeoLocation;
import ru.em70.services.Task;
import ru.em70.services.TaskHandler;
import ru.em70.services.TaskInfo;

@Service
@EnableConfigurationProperties(WeatherTaskHandler.class)
public class WeatherService {
    private final TaskHandler taskHandler;

    public WeatherService(TaskHandler taskHandler) {
        this.taskHandler = taskHandler;
    }

    public String requestWeather(GeoLocation location) throws Exception {
        return this.taskHandler.runJob(new Task(location));
    }

    public TaskInfo getTaskInfo(final String taskId) throws Exception {
        return this.taskHandler.getJobInfo(taskId);
    }
}
