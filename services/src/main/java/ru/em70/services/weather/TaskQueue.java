package ru.em70.services.weather;

import java.util.*;
import java.util.concurrent.locks.ReentrantLock;


public class TaskQueue<E> {
    private final ReentrantLock lock;
    private List<E> list;

    public TaskQueue() {
        list = new ArrayList<>();
        lock = new ReentrantLock();
    }

    public boolean add(E var1) {
        this.lock.lock();

        boolean result;
        try {
            result = list.add(var1);
        } finally {
            this.lock.unlock();
        }

        return result;
    }


    public E poll() {
        this.lock.lock();

        Object result;
        try {
            if (list.size() - 1 < 0) {
                result = null;
            } else {
                result = list.get(0);
                list.remove(0);
            }
        } finally {
            this.lock.unlock();
        }

        return (E) result;
    }

    public int size() {
        this.lock.lock();

        int result;
        try {
            result = this.list.size();
        } finally {
            this.lock.unlock();
        }

        return result;
    }

    public int indexOf(Object var1) {
        int result = -1;

        if (var1 != null) {
            this.lock.lock();

            try {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).equals(var1)) {
                        result = i;
                        break;
                    }
                }
            } finally {
                this.lock.unlock();
            }
        }

        return result;
    }
}