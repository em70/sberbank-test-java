package ru.em70.services.weather;

public class Weather {
    public Weather(Coordinate coordinate, Wind wind, WeatherMain main, WeatherDescription description) {
        this.coordinate = coordinate;
        this.wind = wind;
        this.main = main;
        this.description = description;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public Wind getWind() {
        return wind;
    }

    public WeatherMain getMain() {
        return main;
    }

    public WeatherDescription getDescription() {
        return description;
    }

    public static class Coordinate {
        public Coordinate(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        private double latitude;
        private double longitude;
    }

    public static class Wind {
        public Wind(double speed, double deg) {
            this.speed = speed;
            this.deg = deg;
        }

        public double getSpeed() {
            return speed;
        }

        public double getDeg() {
            return deg;
        }

        private double speed;
        private double deg;
    }

    public static class WeatherMain {
        public WeatherMain(double temp, double humidity,
                           double pressure, double tempMin,
                           double tempMax) {
            this.temp = temp;
            this.humidity = humidity;
            this.pressure = pressure;
            this.tempMin = tempMin;
            this.tempMax = tempMax;
        }

        public double getTemp() {
            return temp;
        }

        public double getHumidity() {
            return humidity;
        }

        public double getPressure() {
            return pressure;
        }

        public double getTempMin() {
            return tempMin;
        }

        public double getTempMax() {
            return tempMax;
        }

        private double temp;
        private double humidity;
        private double pressure;
        private double tempMin;
        private double tempMax;
    }


    public static class WeatherDescription {

        private String name;
        private int code;

        public WeatherDescription(String name, int code) {
            this.name = name;
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public int getCode() {
            return code;
        }
    }

    private Coordinate coordinate;
    private Wind wind;
    private WeatherMain main;
    private WeatherDescription description;
}
