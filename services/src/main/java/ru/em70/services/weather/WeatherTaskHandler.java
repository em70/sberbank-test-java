package ru.em70.services.weather;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import ru.em70.domain.GeoLocation;
import ru.em70.services.Task;
import ru.em70.services.TaskHandler;
import ru.em70.services.TaskInfo;
import ru.em70.services.TaskResult;
import ru.em70.services.exception.EmptyParameterException;
import ru.em70.services.exception.IncorrectParametersException;
import ru.em70.services.jms.ResultMessageSender;

@ConfigurationProperties("service")
public class WeatherTaskHandler implements TaskHandler, WeatherTaskHandlerThread.MessageListener {

    @Autowired
    private ResultMessageSender messageSender;

    private WeatherTaskHandlerThread taskThread;

    public WeatherTaskHandler() {
        init();
    }

    public String runJob(Task task) throws Exception {

        if (task.getParams() == null) {
            throw new EmptyParameterException();
        }

        if (!(task.getParams() instanceof GeoLocation)) {
            throw new IncorrectParametersException("Incorrect GeoLocation parameters");
        }

        return taskThread.addTask(task);
    }

    public TaskInfo getJobInfo(final String sessionId) {
        return taskThread.getTaskInfo(sessionId);
    }

    private void init() {
        taskThread = new WeatherTaskHandlerThread();
        taskThread.setMessageListener(this);
        new Thread(taskThread).start();
    }

    @Override
    public void messageReceived(TaskResult result) {
        messageSender.sendMessage(result);
    }
}
