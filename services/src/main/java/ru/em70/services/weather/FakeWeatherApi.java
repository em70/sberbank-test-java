package ru.em70.services.weather;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

public class FakeWeatherApi implements WeatherApi {
    public FakeWeatherApi() {
        random = new Random(System.currentTimeMillis());
    }

    @Override
    public String getWeatherByPosition(double lat, double lon) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            Weather weather = randomWeather(lat, lon);

            return mapper.writeValueAsString(weather);
        } catch (Exception e) {
            return "{}";
        }
    }

    @Override
    public String getWeatherByCityName(String cityName) {
        return "{}";
    }

    private Weather randomWeather(double lat, double lon) {
        return new Weather(new Weather.Coordinate(lat, lon),
                new Weather.Wind(randomDouble(0, 100), randomDouble(0, 120)),
                new Weather.WeatherMain(randomDouble(0, 100), randomDouble(0, 100),
                        randomDouble(0, 100), randomDouble(0, 100), randomDouble(0, 100)),
                new Weather.WeatherDescription("Малооблачно", (int) randomDouble(1, 4))
        );
    }

    private double randomDouble(double min, double max) {
        return round(min + (max - min) * random.nextDouble(), 2);
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


    private Random random;
}
