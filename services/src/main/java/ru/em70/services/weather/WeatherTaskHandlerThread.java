package ru.em70.services.weather;

import org.springframework.stereotype.Component;

import ru.em70.domain.GeoLocation;
import ru.em70.services.Task;
import ru.em70.services.TaskInfo;
import ru.em70.services.TaskResult;
import ru.em70.services.exception.TaskAddingException;

import java.util.UUID;

@Component
public class WeatherTaskHandlerThread implements Runnable {

    public interface MessageListener {
        void messageReceived(final TaskResult result);
    }

    public WeatherTaskHandlerThread() {
        taskQueue = new TaskQueue<>();
        weatherApi = new FakeWeatherApi();
    }

    /**
     * Подписка на событие завершения задачи
     *
     * @param listener
     */
    public void setMessageListener(MessageListener listener) {
        messageListener = listener;
    }

    /**
     * Добавление задачи в очередь работы
     *
     * @param task
     * @return
     * @throws Exception
     */
    public String addTask(Task task) throws Exception {
        String taskId = getRandomUuid();

        if (!taskQueue.add(new QueueTaskItem(taskId, task))) {
            throw new TaskAddingException();
        }

        return taskId;
    }

    public TaskInfo getTaskInfo(final String taskId) {
        int index = taskQueue.indexOf(new QueueTaskItem(taskId, null));

        return new TaskInfo(taskId, index);
    }

    @Override
    public void run(){
        while (!Thread.interrupted()) {

            try {
                resolveTask(taskQueue.poll());
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendMessage(final TaskResult result) throws Exception{
        if (messageListener != null) {
            messageListener.messageReceived(result);
        } else {
            throw new Exception("messageListener is null");
        }
    }

    private String getRandomUuid() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    private void resolveTask(QueueTaskItem task) throws Exception {
        if (task == null)
            return;

        if (task.task == null)
            return;

        GeoLocation location = (GeoLocation) task.task.getParams();

        String result = weatherApi.getWeatherByPosition(location.getLatitude(), location.getLongitude());
        sendMessage(new TaskResult(task.taskId, result));
    }

    private class QueueTaskItem implements Comparable<QueueTaskItem> {
        private Task task;
        private String taskId;

        public QueueTaskItem(String taskId, Task task) {
            this.taskId = taskId;
            this.task = task;
        }

        public Task getTask() {
            return task;
        }

        public String getTaskId() {
            return taskId;
        }

        @Override
        public int compareTo(QueueTaskItem weatherTask) {
            return this.taskId.compareTo(weatherTask.getTaskId());
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof QueueTaskItem) {
                QueueTaskItem weatherTask = (QueueTaskItem) obj;
                return this.taskId.equals(weatherTask.taskId);
            }

            return false;

        }
    }

    private MessageListener messageListener;
    private TaskQueue<QueueTaskItem> taskQueue;
    private WeatherApi weatherApi;
}
