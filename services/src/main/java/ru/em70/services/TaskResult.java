package ru.em70.services;

/**
 * Represents task result
 */
public class TaskResult {

    private String taskId;
    private String result;

    public TaskResult(String taskId, String result) {
        this.taskId = taskId;
        this.result = result;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getResult() {
        return result;
    }

}
